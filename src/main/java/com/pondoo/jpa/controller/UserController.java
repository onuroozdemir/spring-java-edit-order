package com.pondoo.jpa.controller;


import com.pondoo.jpa.entity.data.AuthLoginRequest;
import com.pondoo.jpa.entity.data.OrderIndexSetRequest;
import com.pondoo.jpa.entity.data.RegisterRequest;
import com.pondoo.jpa.entity.response.GeneralSuccess;
import com.pondoo.jpa.entity.response.TokenResponse;
import com.pondoo.jpa.entity.table.User;
import com.pondoo.jpa.services.UserDaoService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@Api(value = "User Api documentation")
public class UserController {

    @Autowired
    UserDaoService userDaoService;

    @GetMapping("/")
    public String getWelcome() {
        return "Hello";
    }

    @PostMapping("/api/login")
    public TokenResponse login(@RequestBody AuthLoginRequest authLoginRequest) throws Exception {
        return userDaoService.loginAndCreateToken(authLoginRequest);
    }
    @PostMapping("/api/register")
    public GeneralSuccess register(@RequestBody RegisterRequest registerRequest) throws Exception {
        return userDaoService.register(registerRequest);
    }

    @PostMapping("/api/setorder")
        public List<User> setOrderIndex(@RequestBody OrderIndexSetRequest orderIndexSetRequest) throws Exception {
        return userDaoService.setOrderIndex(orderIndexSetRequest);
    }

    @GetMapping("/api/users")
    public List<User> retrieveAllUsersFromJPA(){
        return userDaoService.retrieveAllUsersFrom();
    }
}
