package com.pondoo.jpa.services;

import com.pondoo.jpa.auth.TokenManager;
import com.pondoo.jpa.entity.data.AuthLoginRequest;
import com.pondoo.jpa.entity.data.OrderIndexSetRequest;
import com.pondoo.jpa.entity.data.RegisterRequest;
import com.pondoo.jpa.entity.response.GeneralSuccess;
import com.pondoo.jpa.entity.response.TokenResponse;
import com.pondoo.jpa.entity.table.User;
import com.pondoo.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserDaoService {

    @Autowired
    private TokenManager tokenManager;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    UserRepository userRepository;

    public TokenResponse loginAndCreateToken(AuthLoginRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword())
            );
        } catch (Exception e) {
            throw e;
        }
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setToken(tokenManager.generateToken(authRequest.getUserName()));
        return tokenResponse;
    }

    public GeneralSuccess register(RegisterRequest registerRequest) throws Exception {
        try {
            User user = userRepository.save(new User(registerRequest.getUserName(),
                    registerRequest.getPassword(),
                    registerRequest.getEmail()));
        } catch (Exception e) {
            throw new Exception("Bu username veya email'e sahibiz ama hangisi söylemem");
        }
        return new GeneralSuccess(true,"User saved");
    }

    public List<User> setOrderIndex(OrderIndexSetRequest orderIndexSetRequest) throws Exception {
            if (!userRepository.existsById(orderIndexSetRequest.getId())) {
            throw new Exception("User not exist.");
        }
        List<User> allUser = retrieveAllUsersFrom();
        if (allUser.size() < orderIndexSetRequest.getOrderIndex()) {
            throw new Exception("Out of Index");
        }
        ArrayList<User> droppedList = new ArrayList<User>();
        User selectedUser = null;
        for (User user : allUser) {
            if (user.getId() != orderIndexSetRequest.getId()) {
                droppedList.add(user);
            } else {
                selectedUser = user;
            }
        }
        int droppedListCounter = 0;
        for (int i = 0; i < allUser.size(); i++) {
            int order = i + 1;
            if (order != orderIndexSetRequest.getOrderIndex()) {
                User user = droppedList.get(droppedListCounter);
                user.setOrderIndex(order);
                userRepository.save(user);
                droppedListCounter++;
            } else if (order == orderIndexSetRequest.getOrderIndex()) {
                selectedUser.setOrderIndex(order);
                userRepository.save(selectedUser);
            }
        }
        return retrieveAllUsersFrom();
    }

    public List<User> retrieveAllUsersFrom() {
        return userRepository.findAll(Sort.by(Sort.Direction.ASC, "orderIndex"));
    }
}
