package com.pondoo.jpa.repository;

import com.pondoo.jpa.entity.table.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUserName(String userName);
}
