package com.pondoo.jpa;

import com.pondoo.jpa.entity.table.User;
import com.pondoo.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
@EnableSwagger2
public class JpaApplication {

	@Autowired
	private UserRepository userRepository;

	@PostConstruct
	public void initUsers(){
		List<User> userList = Stream.of(
						new User("onur","1234","ozdemironur1515@gmail.com"),
						new User("omer","1234","ozmer@gmail.com"),
						new User("Jack","1234","jack@gmail.com"),
						new User("Kate","1234","kate@gmail.com"),
						new User("Hurley","1234","hurley@gmail.com"))
				.collect(Collectors.toList());
		userRepository.saveAll(userList);
	}
	public static void main(String[] args) {
		SpringApplication.run(JpaApplication.class, args);
	}



}
