package com.pondoo.jpa.entity.data;

public class AuthLoginRequest {
    String userName;
    String password;

    public AuthLoginRequest() {
    }

    public AuthLoginRequest(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
