package com.pondoo.jpa.entity.data;

public class   OrderIndexSetRequest {
    int id;
    int orderIndex;

    public OrderIndexSetRequest() {
    }

    public OrderIndexSetRequest(int id, int orderIndex) {
        this.id = id;
        this.orderIndex = orderIndex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }
}
