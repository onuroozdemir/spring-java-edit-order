package com.pondoo.jpa.entity.response;

public class GeneralSuccess {
    boolean status;
    String message;

    public GeneralSuccess(boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public GeneralSuccess() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
