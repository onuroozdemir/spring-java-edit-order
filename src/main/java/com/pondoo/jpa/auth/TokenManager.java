package com.pondoo.jpa.auth;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class TokenManager {

    private static final String secretKey = "onurozdemirpondoo1515omerfarukburdurturkiyeavrupaortadogu";
    private static final int validityDuration = 60 * 1000 * 60 * 24;

    public String generateToken(String username) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims,username);
    }
    private String createToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + validityDuration))
                .signWith(SignatureAlgorithm.HS256, secretKey).compact();
    }

    public boolean tokenValidate(String token) {
        if(getUserFromToken(token) != null && !isExpired(token)){
            return true;
        }
        return false;
    }
    public String getUserFromToken(String token) {
        return getClaims(token).getSubject();
    }

    public long getDate() {
        return System.currentTimeMillis();
    }
    public boolean isExpired(String token){
        return getClaims(token).getExpiration().before(new Date(System.currentTimeMillis()));
    }

    public Claims getClaims(String token){
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
    }
}
